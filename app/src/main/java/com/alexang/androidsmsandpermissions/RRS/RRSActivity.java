package com.alexang.androidsmsandpermissions.RRS;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com.alexang.androidsmsandpermissions.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class RRSActivity extends Activity {

    private static final String URL = "https://feeds.yle.fi/uutiset/v1/recent.rss?publisherIds=YLE_NOVOSTI";
    private ListView listViewRRS;
    private List<String> titles;
    private List<String> links;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rrs);

        titles = new ArrayList<>();
        links = new ArrayList<>();

        listViewRRS = (ListView) findViewById(R.id.list_rrs);
        listViewRRS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Uri uri = Uri.parse(links.get(position));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        new ProcessInBackground().execute();
    }


    public InputStream getInputStream(final URL url) {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            Log.d("RRS", e.toString());
            return null;
        }
    }

    public class ProcessInBackground extends AsyncTask<Integer, Integer, Exception> {

        ProgressDialog progressDialog = new ProgressDialog(RRSActivity.this);
        Exception exception;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading...");
        }

        @Override
        protected Exception doInBackground(Integer... params) {
            try {
                URL url = new URL(URL);
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(false);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(getInputStream(url), "UTF_8");

                //Inside <item></item> tag. If inside then do..
                boolean insideItem = false;
                //This evenType says where we are. Open or Closed tag?
                int eventType = xpp.getEventType();

                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        //Returns the specific TAG name
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        } else if (xpp.getName().equalsIgnoreCase("title") && insideItem) {
                            titles.add(xpp.nextText());
                        } else if (xpp.getName().equalsIgnoreCase("link") && insideItem) {
                            links.add(xpp.nextText());
                        }
                    } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        insideItem = false;
                    }
                    eventType = xpp.next();
                }

            } catch (IOException | XmlPullParserException e) {
                exception = e;
            }
            return exception;
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(RRSActivity.this, android.R.layout.simple_list_item_1, titles);
            listViewRRS.setAdapter(adapter);
            progressDialog.dismiss();
        }
    }


}
