package com.alexang.androidsmsandpermissions.Services;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.alexang.androidsmsandpermissions.R;

public class ServiceActivity extends Activity implements MusicStoppedListener {
    private static final String AUDIO_TAG = "AUDIO";

    private ImageView mImageSound;
    private static final String SONG_LINK = "https://dl.dropbox.com/s/5ey5xwb7a5ylqps/games_of_thrones.mp3?dl=0";
    private boolean isMusicPlaying = false;
    private Intent serviceIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        mImageSound = (ImageView) findViewById(R.id.image_sound_service);
        serviceIntent = new Intent(ServiceActivity.this, MyPlayService.class);
        mImageSound.setImageResource(R.drawable.img_play_outlined_music);

        ApplicationClass.context = ServiceActivity.this;

        mImageSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isMusicPlaying) {
                    playAudio();
                    mImageSound.setImageResource(R.drawable.img_play_outlined_music);
                    isMusicPlaying = true;
                } else {
                    stopPlayService();
                    mImageSound.setImageResource(R.drawable.img_play_music);
                    isMusicPlaying = false;
                }

            }
        });
    }


    private void playAudio() {
        serviceIntent.putExtra("audioLink", SONG_LINK);
        try {
            startService(serviceIntent);
        } catch (SecurityException e) {
            Log.d(AUDIO_TAG, e.toString());
        }
    }

    private void stopPlayService() {
        try {
            stopService(serviceIntent);
        } catch (SecurityException e) {
            Log.d(AUDIO_TAG, e.toString());
        }
    }

    @Override
    public void onMusicStopped() {
        mImageSound.setImageResource(R.drawable.img_play_music);
        isMusicPlaying = false;
    }
}