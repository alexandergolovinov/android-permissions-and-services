package com.alexang.androidsmsandpermissions.Services;

public interface MusicStoppedListener {
    void onMusicStopped();
}
