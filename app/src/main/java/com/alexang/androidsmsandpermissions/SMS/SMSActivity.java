package com.alexang.androidsmsandpermissions.SMS;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.alexang.androidsmsandpermissions.R;

public class SMSActivity extends Activity {

    private static final int UNIQUE_CODE_SMS_REQUEST = 1;
    private static final String SENT = "SMS SENT";
    private static final String DELIVERED = "SMS_DELIVERED";

    private PendingIntent sentPendingIntent;
    private PendingIntent deliveredPendingIntent;
    private BroadcastReceiver smsSendReceiver;
    private BroadcastReceiver smsDeliveredReceiver;

    private EditText mEditTextMessage;
    private EditText mEditTextPhoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        mEditTextMessage = (EditText) findViewById(R.id.edit_message_sms);
        mEditTextPhoneNumber = (EditText) findViewById(R.id.edit_phone_num);
        sentPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        deliveredPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

    }

    @Override
    protected void onResume() {
        super.onResume();
        smsSendReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(SMSActivity.this, "SMS Sent sucessfully!", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(SMSActivity.this, "Generic Failure!", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(SMSActivity.this, "No Service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(SMSActivity.this, "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(SMSActivity.this, "Radio OFF!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        };

        smsDeliveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(SMSActivity.this, "SMS Delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(SMSActivity.this, "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        };

        /*This registered Broadcast Receivers are listen to the internal system event (SENT or DELIVERED), which is
        triggered after PendingIntent. PendingIntent then shouts the Intent(...) and this listener listens to it and acts upon it.*/
        registerReceiver(smsSendReceiver, new IntentFilter(SENT));
        registerReceiver(smsDeliveredReceiver, new IntentFilter(DELIVERED));
    }

    public void executeSendSMSRequest(View view) {
        if (ContextCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SMSActivity.this,
                    new String[]{Manifest.permission.SEND_SMS},
                    UNIQUE_CODE_SMS_REQUEST);
        } else {
            sendSMS();
        }
    }

    public void sendSMS() {
        String message = mEditTextMessage.getText().toString();
        String phoneNum = mEditTextPhoneNumber.getText().toString();
        if (!TextUtils.isEmpty(message) || !TextUtils.isEmpty(phoneNum)) {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNum, null, message, sentPendingIntent, deliveredPendingIntent);
        } else {
            Toast.makeText(SMSActivity.this, "Please fill the fields", Toast.LENGTH_SHORT).show();
        }
    }
}
