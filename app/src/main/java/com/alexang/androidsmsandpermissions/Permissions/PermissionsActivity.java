package com.alexang.androidsmsandpermissions.Permissions;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.alexang.androidsmsandpermissions.R;

public class PermissionsActivity extends Activity {

    private static final int UNIQUE_REQUEST_CODE = 1;

    private Button mButtonPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        mButtonPermissions = (Button) findViewById(R.id.btn_permissions);

        mButtonPermissions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*To check if you have a permission, call the ContextCompat.checkSelfPermission() method*/
                if (ContextCompat.checkSelfPermission(PermissionsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    //No explanation. Just Ask one more time to grant the permission
                    ActivityCompat.requestPermissions(PermissionsActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            UNIQUE_REQUEST_CODE);
                } else {
                    Toast.makeText(PermissionsActivity.this, "Granted Permissions", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * Right after the request permission, the method goes here.
     *
     * @param requestCode  unique code goes along with request permission
     * @param permissions  array
     * @param grantResults array of results
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //If TRUE, then we know that the method was responded to the method we asked before
        if (requestCode == UNIQUE_REQUEST_CODE) {

            //If user permits the request
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(PermissionsActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();

                //If user denies the request then ask ONE more time with explanation. If not, then...
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                /**
                 * Android provides a utility method, shouldShowRequestPermissionRationale(),
                 * that returns TRUE if the user has previously denied the request,
                 * and returns FALSE if a user has denied a permission and selected the "Don't ask again" option
                 */
                if (ActivityCompat.shouldShowRequestPermissionRationale(PermissionsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setMessage("The permission is very important to use for this application. Please permit it.")
                            .setTitle("Important permission required!")
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(PermissionsActivity.this, "Cannot process the app feature", Toast.LENGTH_SHORT).show();

                                }
                            })
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(PermissionsActivity.this,
                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                            UNIQUE_REQUEST_CODE);
                                }
                            });
                    dialog.show();

                } else {
                    //If user says "never ask again"
                    Toast.makeText(PermissionsActivity.this, "Wont be asked anymore", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
